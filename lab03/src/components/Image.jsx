import React from 'react';
import '../App.scss'

import {Link, useParams} from 'react-router-dom';

const Image = ({photos},props) => {
    const params = useParams();
    return (
        <div className="img">
            <img src={photos.find(e => e.id === parseInt(params.id)).url} alt=""/>
            <Link to={'/'} className={'link'}>x</Link>
        </div>
    );
};

export default Image;