import React, {useState, useEffect} from "react";
import PhotoItem from "./PhotoItem";


import {Routes ,Route, Link} from "react-router-dom";
import Image from "./Image";

function PhotoApp() {
    const [photos, setPhotos] = useState(null);
    const [photosf, setPhotosf] = useState(null);

    const [isLoad, setIsLoad] = useState(false);

    const [fromTitle, setFromTitle] = useState(0);
    const [toTitle, setToTitle] = useState(7);

    const [count, setCount] = useState(50);

    const [page, setPage] = useState([]);

    const [num, setNum] = useState(0);

    const [search, setSearch] = useState('');

    const [style, setStyle] = useState(50);

    const [mysort, setSort] = useState('title1');

    useEffect(() => {
        fetchPhotos();
    }, [])

    const fetchPhotos = async () => {
        let response = await fetch("https://jsonplaceholder.typicode.com/photos");
        let photos = await response.json();
        setPhotos(photos);
        setIsLoad(true);
        setPage(photos.filter((photo) =>
            photo.title.split(" ").length <= toTitle &&
            photo.title.split(" ").length >= fromTitle).map((e, index) => index));
        setPhotosf(photos.filter((photo) =>
            photo.title.split(" ").length <= toTitle &&
            photo.title.split(" ").length >= fromTitle));
    }

    function load(toTitle, fromTitle, search){
        setTimeout(() => {setIsLoad(true)}, 200)
        sorts();
        setPage(photos.filter((photo) =>
            photo.title.split(" ").length <= toTitle &&
            photo.title.split(" ").length >= fromTitle && photo.title.search(search) !== -1).map((e, index) => index))
        setPhotosf(photos.filter((photo) =>
            photo.title.split(" ").length <= toTitle &&
            photo.title.split(" ").length >= fromTitle &&
            photo.title.search(search) !== -1));
        setNum(0);
    }

    function sorts(){
        let newAr = [...photosf].sort((a, b) => a['title'].localeCompare(b['title']));
        if (mysort == 'title1'){
            newAr.reverse();
        }
        console.log(mysort)
        setPhotosf(newAr)
    }

    return (
        <>
            <div>
                {photos &&
                    <div className="setting">
                        <input className={'setting_search'} placeholder="Search" type="text" onChange={(e) => {setSearch(e.target.value);setIsLoad(false);load(toTitle, fromTitle, e.target.value);}} />
                        <input className={'setting_search'} placeholder="Height" type="range" onChange={(e) => {setStyle(e.target.value)}} />
                        <div className="setting_inp">
                            <input type="text" placeholder="от" value={fromTitle} onChange={(e) => {load(toTitle, e.target.value, search);setFromTitle(e.target.value);setIsLoad(false);}}/>
                            <input type="text" placeholder="до" value={toTitle} onChange={(e) => {load(e.target.value, fromTitle, search);setToTitle(e.target.value);setIsLoad(false);}}/>
                        </div>
                        <div className="setting_s">
                            <select value={mysort} onChange={e => {setSort(e.target.value); load(toTitle, fromTitle, search, true); sorts(); setIsLoad(false);}}>
                                <option value="title1">По назві</option>
                                <option value="title2">По назві (-)</option>
                            </select>
                        </div>
                        <div className="setting_s">
                            <select value={count} onChange={e => {load(toTitle, fromTitle, search, true);setCount(e.target.value); setIsLoad(false);}}>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="500">500</option>
                                <option value="2000">2000</option>
                                <option value={photosf.length}>All</option>
                            </select>
                        </div>
                        <div className={'num_page'}>
                            {
                                page.slice(0, Math.ceil(photosf.length/count)).map(ind =>
                                    <span key={ind} onClick={() => {setNum(ind);}} className={ind !== num ? 'span_P' : 'span_P span_R'}> {ind+1} </span>
                                )
                            }
                        </div>
                    </div>
                }
                {isLoad === true ?
                    <div>
                        <div>
                            {
                                photosf.length > 0
                                    ? photosf.slice(num*count, (num+1)*count).map((photo) => <PhotoItem st={style} photo={photo} key={photo.id}/>)
                                    : <span style={{display: 'inline-block',width: '100%', textAlign: 'center', fontSize: '20px'}}>Нема :(</span>
                            }
                        </div>
                    </div> : <div className="loading">Loading...</div>}
            </div>
            <Routes>
                <Route path="/:id" element={<Image photos={photos}/>} />
            </Routes>
        </>
    );

}

export default PhotoApp;
