import React from "react";

import Image from './Image'
import {Link, useParams} from "react-router-dom";

function PhotoItem({photo, st, ...props}) {
    return (
        <div className="photo" style={{height: `${st}px`}}>
            <Link className="photo_a" to={`/${photo.id}`}>
                <img src={photo.thumbnailUrl} alt={'sdf'}/>
            </Link>
            <div className="photo_title">
                {photo.title}
            </div>
        </div>
    );


}

export default PhotoItem;