import './App.scss';
import PhotoApp from "./components/PhotoApp";
import Image from './components/Image'
import {Route, Routes} from "react-router-dom";

function App() {
    return (
        <div className="App">
            <PhotoApp/>
        </div>
    );
}

export default App;
