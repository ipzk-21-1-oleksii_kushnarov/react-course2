import './App.scss';
import ProductApp from "./components/ProductApp";

function App() {
  return (
    <div className="App">
      <ProductApp />
    </div>
  );
}

export default App;
