import React, {useContext, useState} from 'react';
import {ProductsContext} from "../context/products-context";

const ProductFrom = () => {
    const {dispatch} = useContext(ProductsContext);
    const [productTitle, setProductTitle] = useState('');

    const onSubmit = (e) => {
        e.preventDefault()
        dispatch({
            type: 'add',
            payload: productTitle,
        })
        setProductTitle('');
    }

    return (
        <form onSubmit={onSubmit}>
            <input type="text" value={productTitle} onChange={(e) => setProductTitle(e.target.value)}/>
            <input type="submit" value="Додати"/>
        </form>
    );
};

export default ProductFrom;