import React, {useContext} from 'react';
import {ProductsContext} from "../context/products-context";

const ProductItem = ({id, title}) => {
    const {dispatch} = useContext(ProductsContext)

    return (
        <div className="product">
            <span>{title}</span>
            <button onClick={() => {
                dispatch({type: 'remove', payload: id})
            }}> Del
            </button>
        </div>
    );
};

export default ProductItem;