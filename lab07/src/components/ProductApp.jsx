import React, {useReducer} from 'react';
import reducer from '../redusers/products'
import ProductFrom from "./ProductFrom";
import ProductList from "./ProductList";

import {ProductsContext} from '../context/products-context'

const ProductApp = () => {
    const [products, dispatch] = useReducer(reducer, []);

    return (
        <ProductsContext.Provider value={{
            products, dispatch
        }}>
            <div className="productsApp">
                <ProductFrom />
                <ProductList />
            </div>
        </ProductsContext.Provider>
    );
};

export default ProductApp;