import React, {useContext} from 'react';
import ProductItem from "./ProductItem";
import {ProductsContext} from "../context/products-context";
import {TransitionGroup, CSSTransition} from "react-transition-group";

const ProductList = () => {
    const {products} = useContext(ProductsContext)

    return (
        <div className="products">
            {products.length
                ? <TransitionGroup>
                    {products.map((product) =>
                        <CSSTransition
                            key={product.id}
                            timeout={300}
                            classNames="item"
                        >
                            <ProductItem {...product}/>
                        </CSSTransition>
                    )}
                </TransitionGroup>
                : <h4 style={{textAlign: 'center', marginTop: 10}}>Нема 😥</h4>
            }
        </div>
    );
};

export default ProductList;