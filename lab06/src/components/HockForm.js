import React, {useState} from 'react';
import {useForm} from 'react-hook-form'

const HockForm = () => {
    const { register, handleSubmit, watch, formState: { errors }, reset} = useForm();
    const onSubmit = data => {
        console.log(data);
        reset();
        setPlace([1]);
    };

    const [counters, setCounters] = useState([
        'Київ', 'Житомир', 'Львів', 'Харьків', 'Тернопіль'
    ]);

    const [place, setPlace] = useState([1]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="pad">
                <div className="form_line">
                    <div>Маршрут</div>

                    <select {...register("counters")}>
                        {counters.map(counter =>
                            <option key={counter} value={counter}>{counter}</option>
                        )}
                    </select>
                </div>
                <div className="form_line">
                    <div>Вид відправлення</div>

                    <select {...register("typeOfShipment")}>
                        <option value="Вантажні">Вантажні</option>
                        <option value="Палети">Палети</option>
                    </select>
                </div>

                <div className="form_line">
                    <div className="form_line_gap">
                        <div>Характеристика місць</div>
                        {
                            place.map(e =>
                                <div key={e} className="form_line_grid">
                                    <span>Кількість</span>
                                    <span>Оголошена вартість</span>
                                    <span>Вага</span>
                                    <span>Довжина</span>
                                    <span>Ширина</span>
                                    <span>Висота</span>
                                    <input type="number" defaultValue={1} {...register(`countInfo${e}`)}/>
                                    <div className="plus"><input type="text" {...register(`priceInfo${e}`)}/>грн</div>
                                    <div className="plus"><input type="text" {...register(`weightInfo${e}`)}/>кг</div>
                                    <input type="text" {...register(`priceInfo${e}`)}/>
                                    <input type="text" {...register(`weightInfo${e}`)}/>
                                    <div className="plus">
                                        <input type="text" {...register(`priceInfo${e}`)}/>
                                        {place.length > 1 &&
                                            <div onClick={() => setPlace(place.filter(el => el !== e))}>x</div>
                                        }
                                    </div>
                                </div>
                            )
                        }
                        <div className="decor" onClick={() => setPlace([...place, place[place.length-1] + 1])}>Додати місце</div>
                    </div>
                </div>

                <div className="form_line2">
                    <div className="sd">Послуга "Пакування"</div>

                    <input type="checkbox" {...register("pac")}/>
                </div>
                <div className="form_line2">
                    <div className="sd">Послуга "Підйом на поверх"</div>

                    <div className="form_line2 asa">
                        <input type="text" {...register("up")}/>
                        <span>Кількість поверхів</span>
                        <div>Ліфт</div>
                        <input type="checkbox" {...register("back")}/>
                    </div>
                </div>
                <div className="form_line2">
                    <div className="sd">Послуга "Зворотна доставка"</div>

                    <input type="checkbox" {...register("back")}/>
                </div>
            </div>




            {/*<input {...register("exampleRequired", { required: true })} />*/}
            {/*{errors.exampleRequired && <span>This field is required</span>}*/}

            <div className="form2_btn">
                <div className="form2_50">
                    <input type="submit" />
                    <span className="decor" onClick={() => {reset();setPlace([1]);}}>Очистити</span>
                </div>
            </div>
        </form>
    );
};

export default HockForm;