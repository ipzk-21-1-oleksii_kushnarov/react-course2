import React, {useState} from 'react';
import '../App.scss'

const MyForm = () => {
    const [inputs, setInputs] = useState({
        name: '',
        mail: '',
        topic: '',
        message: '',
    });

    const submit = (e) => {
        e.preventDefault()
        console.log(inputs)
        setInputs({name: '', mail: '', topic: '', message: ''})
    }

    return (
        <form onSubmit={submit}>
            <div className="pad">
                <input type="text" value={inputs.name} placeholder="Ім'я"
                       onChange={(e) => setInputs({...inputs, name: e.target.value})}/>
                <input type="text" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}" value={inputs.mail}
                       placeholder="Пошта*"
                       onChange={(e) => setInputs({...inputs, mail: e.target.value})}/>
                <input type="text" required value={inputs.topic} placeholder="Тема*"
                       onChange={(e) => setInputs({...inputs, topic: e.target.value})}/>
                <textarea value={inputs.message} placeholder="Повідомлення"
                          onChange={(e) => setInputs({...inputs, message: e.target.value})}/>
                <span>Поля відмічені * мають бути обов'язково заповненими</span>
                <div className="button-block">
                    <button>Відправити</button>
                </div>
            </div>
        </form>
    );
};

export default MyForm;