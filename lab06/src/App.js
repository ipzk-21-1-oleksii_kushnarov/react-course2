import './App.scss';
import MyForm from "./components/MyForm";
import HockForm from "./components/HockForm";
import {Routes, Route, Link} from "react-router-dom";

function App() {
  return (
    <div className="App">

        <div className="menu">
            <Link to="/1">
                <button>
                    MyForm
                </button>
            </Link>
            <Link to="/2">
                <button>
                    HookForm
                </button>
            </Link>
        </div>

        <Routes>
            <Route path="/" element={<MyForm/>}/>
            <Route path="/1" element={<MyForm/>}/>
            <Route path="/2" element={<HockForm/>}/>
            <Route path="*" element={<div>Nema</div>}/>
        </Routes>
    </div>
  );
}

export default App;
