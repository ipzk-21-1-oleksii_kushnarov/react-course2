import logo from './logo.svg';

import './App.css';
import MyCounter from "./components/MyCounter";
import MyListCounter from "./components/MyListCounter";
import Product from "./components/Product";
import Cart from "./components/Cart";
import Game from "./components/Game";

function App() {
    return (
        <div className="App">
            <p>Завдання №1</p>
            <MyListCounter/>
            <br/><p>Завдання №2</p>
            <Product/>
            <br/><p>Завдання №3</p>
            <Cart/>
            <br/><p>Завдання №4</p>
            <Game/>

        </div>
  );
}

export default App;
