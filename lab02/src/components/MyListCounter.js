import React from 'react';
import './MyCounter'
import '../App.css'
import MyCounter from "./MyCounter";

const MyListCounter = () => {
    const counters = [
        {id: 1, initial: 6, min: -5, max: 10},
        {id: 2, initial: 5},
        {id: 3}
    ];

    return (
        <div className={'counters'}>
            {counters.map(e =>
                <MyCounter initial={e.initial} min={e.min} max={e.max} key={e.id}/>
            )}
        </div>
    );
};

export default MyListCounter;