import React, {useState} from 'react';
import '../App.css'

const MyCounter = ({initial = 10, max = 10, min = -10}, props) => {
    const [count, setCount] = useState(initial)

    function increment() {
        if (count + 1 <= max)
            setCount(count + 1);
    }

    function decrement() {
        if (count - 1 >= min)
            setCount(count - 1);
    }

    return (
        <div className={'counter'}>
            <span>Поточний рахунок: {count}</span>
            <div className={'counter_btn_block'}>
                <button className={'counter_btn'} onClick={increment}>+</button>
                <button className={'counter_btn'} onClick={decrement}>-</button>
                <button className={'counter_btn'} onClick={() => {
                    setCount(initial)
                }}>Reset
                </button>
            </div>
        </div>
    );
};

export default MyCounter;