import React, {useState} from 'react';

const Product = () => {
    const [product, setProduct] = useState([
        {id: 1, name: 'Mouse'},
        {id: 2, name: 'Keyboard'},
        {id: 3, name: 'Monitor'},
    ]);

    const [name, setName] = useState('');

    return (
        <form onSubmit={event => (event.preventDefault())} className={'form_Product'}>
            <div>
                <input type="text" placeholder={'Product Name'} value={name} onChange={(e) => setName(e.target.value)} className={'product_inp'}/>
                <button onClick={() => {setProduct([...product, {id: product[product.length - 1].id+1, name: name}]); setName('');}} className={'product_add'}>Додати</button>
            </div>
            <ul>
                {product.map(item =>
                    <li>
                        <span>{item.name}</span>
                        <button onClick={() => {setProduct(product.filter(p => p.id !== item.id))}} className={'product_del'}>del</button>
                    </li>
                )}
            </ul>
        </form>
    );
};

export default Product;