import React, {useState} from 'react';
import '../App.css'

const Game = () => {
    const [num, setNum] = useState(0);
    const [text, setText] = useState('');

    const [myStyle, setMyStyle] = useState(true);

    const [count, setCount] = useState(0);

    const [result, setResult] = useState('');

    const [info, setInfo] = useState([]);

    function start(){
        let rand = Math.floor(Math.random() * 1001)
        setNum(rand);
        setInfo([]);
        setMyStyle(false)
        setResult('')
        setCount(0);
    }


    function check(){
        setCount(count+1)
        if(num === parseInt(text))
        {
            setResult('Good Job!');
            setMyStyle(true);
        }
        else{
            if(count+1 === 10){
                setResult('Game Over!')
                setMyStyle(true);
            }
            else{
                if(parseInt(text) < num){
                    setInfo([...info, `N > ${parseInt(text)}`])
                }
                else {
                    setInfo([...info, `N < ${parseInt(text)}`])
                }
            }
        }
        setText('')

    }

    function myUl(){
        return (
            <ul className={'game_ul'}>
                {info.map(item =>
                    <li>{item}</li>
                )}
            </ul>
        )
    }

    return (
        <div className={'game'}>
            <div className={'game_btn'}>
                <button disabled={!myStyle} onClick={start}>New Game</button>
                <input disabled={myStyle} type="text" value={text} onChange={e => setText(e.target.value)}/>
                <button disabled={myStyle} onClick={check}>Check</button>
            </div>
            <span>
                Information:
                {info.length > 0 ? myUl() : ''}
            </span>
            <span>Attempts: {count}</span>
            <span>Result: {result}</span>
        </div>
    );
};

export default Game;