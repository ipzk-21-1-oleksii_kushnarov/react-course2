import React, {useState} from 'react';
import '../App.css'

const Cart = () => {
    const [product, setProduct] = useState([
        {id: 1, name: 'Constructor LEGO', price: 300,  min: 1, max: 7, count: 1},
        {id: 2, name: 'Train Station', price: 200,  min: 1, max: 7, count: 1},
        {id: 3, name: 'Hot Wheels Track', price: 150,  min: 1, max: 7, count: 1}
    ]);

    function increment(id){
        const newProd = [];
        product.forEach(item => {
            if (item.id === id){
                let temp = item;
                if (++temp.count <= item.max)
                    newProd.push(temp)
                else{
                    --temp.count
                    newProd.push(temp)
                }
            }
            else{
                newProd.push(item)
            }
            setProduct(newProd)
        })
    }

    function decrement(id){
        const newProd = [];
        product.forEach(item => {
            if (item.id === id){
                let temp = item;
                if (--temp.count >= item.min)
                    newProd.push(temp)
                else{
                    ++temp.count
                    newProd.push(temp)
                }
            }
            else{
                newProd.push(item)
            }
            setProduct(newProd)
        })
    }

    return (
        <div>
            <h1>Cart</h1>
            <table className={'myTable'}>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total</th>
                </tr>
                {product.map(item =>
                    <tr>
                        <td>{item.name}</td>
                        <td>{item.price}</td>
                        <td>
                            <button className={'counter_btn'} onClick={() => increment(item.id)}> + </button>
                            <span>{item.count}</span>
                            <button className={'counter_btn'} onClick={() => decrement(item.id)}> - </button>
                        </td>
                        <td>{item.price * item.count}</td>
                    </tr>
                )}
                <tr>
                    <th colSpan={2}>Totals</th>
                    <th>{product.map(item => item.count).reduce((prev, curr) => prev + curr)}</th>
                    <th>{product.map(item => item.price * item.count).reduce((prev, curr) => prev + curr)}</th>
                </tr>
            </table>
        </div>
    );
};

export default Cart;
