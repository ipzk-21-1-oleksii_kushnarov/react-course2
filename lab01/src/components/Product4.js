
import Product4Item from "./Product4Item";

const Product4 = (props) => {

    return (
        <div>
            <h1>{props.title}</h1>
            <div className={"allItem"}>
                {props.item.map(item =>
                    <Product4Item item={item} key={item.id}/>
                )}
            </div>
        </div>
    );
};
export default Product4;