
import '../App.css'
import img from '../img/k1.jpg'

const Product4Item = (props) => {
    return (
        <div className={"item"}>
            <img src={img} alt="img"/>
            <p>{props.item.title}</p>
            <span>{props.item.price}</span>
        </div>
    );
};

export default Product4Item;
