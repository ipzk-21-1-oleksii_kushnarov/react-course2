import React,{useState} from "react";
function MyProdut(){
    const [color, setColor] = useState("red");

    return(
        <div>
            <p>This is {color} Product</p>
            <select onChange={(e) =>setColor(e.target.value)}>
                <option value="red">Red</option>
                <option value="green">Green</option>
                <option value="blue">Blue</option>
            </select>
        </div>
    )
}

export default MyProdut;