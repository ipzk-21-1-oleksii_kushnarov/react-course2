import './App.css';
import MyTable from "./components/MyTable";
import Product from "./components/Product";
import MySelect from "./components/MySelect";
import MyProdut from "./components/MyProdut";
import Product4 from "./components/Product4";

function App() {
    const product1 = {name: "Mouse"}
    const product2 = {name: "Keyboard"}
    const cities =[
        {id:1, name:"Kyiv"},
        {id:2, name:"Zhytomyr"},
        {id:3, name:"Vinnitsa"}
    ]
    const item = [
        {id: 1, title: 'Портативна радіостанція', price: "24 000 ₴"},
        {id: 2, title: 'Антенний аналізатор 100', price: "35 000 ₴"},
        {id: 3, title: 'Антенний аналізатор 500', price: "22 000 ₴"},
        {id: 4, title: 'Ноутбук 34 ', price: "7 000 ₴"},
        {id: 5, title: 'Ноутбук 7 ', price: "21 000 ₴"},
        {id: 6, title: 'Безпроводні навушники', price: "27 000 ₴"}
    ]

    const item2  = [
        {id: 1, title: 'Рація', price: "24 000 ₴"},
        {id: 2, title: 'Рація високочастотна', price: "34 000 ₴"},
        {id: 3, title: 'Рація 495', price: "22 000 ₴"},
        {id: 4, title: 'Портативна радіостанція 32', price: "41 000 ₴"},
        {id: 5, title: 'Портативна радіостанція 1', price: "18 000 ₴"},
        {id: 6, title: 'Портативна радіостанція 4', price: "19 000 ₴"}
    ]

    return (
        <div className="App">
            <header className="App-header">
                <p>Завдання1</p>
                <MyTable></MyTable>
                <p>Завдання2</p>
                <Product product={product1}/>
                <Product product={product2}/>
                <p>Завдання3</p>
                <MySelect cities={cities}/>

                <MyProdut></MyProdut>
                <p>Завдання4</p>
                <Product4 item={item} title={'Останні'}/>
                <Product4 item={item2} title={'ТОП ТОВАРІВ'}/>
            </header>
        </div>
    );
}

export default App;
