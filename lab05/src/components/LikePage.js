import { Item } from "./Item";
import "./favorite.css"

export const LikePage = ({products, state, dispatch}) => {

    const resultArray = products.filter((item) => {return state.liked.some((item2) => item2.id === item.id)});

     return(
    <div className="flex justify-around">
        <div className="myGrid">
            {resultArray.map((like) => <Item key={like.id} product={like} dispatch={dispatch} state={state}/>)}
        </div>
    </div>
    )
}