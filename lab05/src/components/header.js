import { Link, Outlet } from "react-router-dom"
const like = require("../icons/favorite.png")

export const Header = () => {
    return(
        <>
            <div className="h-8 bg-black sticky top-0 w-full z-50">
                <nav className="flex justify-around">
                    <Link to="/" className="text-white italic text-2xl ">GO HOME</Link>
                    <Link to="/like"><img className="ico" alt="" src={like}/></Link>
                </nav>
                
            </div>
            <Outlet/>
        </>
    )
}